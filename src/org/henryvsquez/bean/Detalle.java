package org.henryvsquez.bean;

public class Detalle {
	private Integer idDetalle;
	private Factura idFactura;
	private Producto idProducto;
	private Integer cantidadDetalle;
	private Double precioDetalle;
	
	
	public Detalle(){
		
	}


	public Detalle(Integer idDetalle, Factura idFactura, Producto idProducto, Integer cantidadDetalle,
			Double precioDetalle) {

		this.idDetalle = idDetalle;
		this.idFactura = idFactura;
		this.idProducto = idProducto;
		this.cantidadDetalle = cantidadDetalle;
		this.precioDetalle = precioDetalle;
	}
	
	

	public Detalle(Factura idFactura, Producto idProducto, Integer cantidadDetalle, Double precioDetalle) {
		super();
		this.idFactura = idFactura;
		this.idProducto = idProducto;
		this.cantidadDetalle = cantidadDetalle;
		this.precioDetalle = precioDetalle;
	}


	public Integer getIdDetalle() {
		return idDetalle;
	}


	public void setIdDetalle(Integer idDetalle) {
		this.idDetalle = idDetalle;
	}


	public Factura getIdFactura() {
		return idFactura;
	}


	public void setIdFactura(Factura idFactura) {
		this.idFactura = idFactura;
	}


	public Producto getIdProducto() {
		return idProducto;
	}


	public void setIdProducto(Producto idProducto) {
		this.idProducto = idProducto;
	}


	public Integer getCantidadDetalle() {
		return cantidadDetalle;
	}


	public void setCantidadDetalle(Integer cantidadDetalle) {
		this.cantidadDetalle = cantidadDetalle;
	}


	public Double getPrecioDetalle() {
		return precioDetalle;
	}


	public void setPrecioDetalle(Double precioDetalle) {
		this.precioDetalle = precioDetalle;
	}
	
	
}

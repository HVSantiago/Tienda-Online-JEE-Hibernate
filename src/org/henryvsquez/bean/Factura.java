package org.henryvsquez.bean;

import java.util.Date;

public class Factura {
	private Integer idFactura;
	private Usuario idUsuario;
	private String fechaFacturacion;
	
	public Factura(){
		
	}
	
	public Factura(Integer idFactura, Usuario idUsuario, String fechaFacturacion) {

		this.idFactura = idFactura;
		
		this.idUsuario = idUsuario;
		
		this.fechaFacturacion = fechaFacturacion;
	}
	
	public Factura(Usuario idUsuario, String fechaFacturacion) {
		this.idUsuario = idUsuario;
		
		this.fechaFacturacion = fechaFacturacion;
	}

	public Integer getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Integer idFactura) {
		this.idFactura = idFactura;
	}

	public Usuario getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Usuario idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getFechaFacturacion() {
		return fechaFacturacion;
	}

	public void setFechaFacturacion(String fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}
	
	
	
}

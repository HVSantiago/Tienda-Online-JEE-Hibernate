package org.henryvsquez.bean;

public class Notificacion {
	private Integer idNotificacion;
	private Producto idProducto;
	private String notificacion;
	
	
	public Notificacion(){
		
	}
	
	
	public Notificacion(Producto idProducto, String notificacion) {
		this.idProducto = idProducto;
		this.notificacion = notificacion;
	}


	public Notificacion(Integer idNotificacion, Producto idProducto, String notificacion) {
		this.idNotificacion = idNotificacion;
		this.idProducto = idProducto;
		this.notificacion = notificacion;
	}
	
	
	public Integer getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(Integer idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public Producto getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Producto idProducto) {
		this.idProducto = idProducto;
	}
	public String getNotificacion() {
		return notificacion;
	}
	public void setNotificacion(String notificacion) {
		this.notificacion = notificacion;
	}
	
	
}

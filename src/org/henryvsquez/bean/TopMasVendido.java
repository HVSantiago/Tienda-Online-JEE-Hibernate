package org.henryvsquez.bean;

public class TopMasVendido {
	private Integer idProducto;
	private String nombreProducto;
	private String descripcionProducto;
	private String imagen;
	private Double precio;
	private Categoria idCategoria;
	private Integer stock;

	
	public TopMasVendido(){
		
	}
	public TopMasVendido(Integer idProducto, String nombreProducto, String descripcionProducto,		
			String imagen, Double precio,Categoria idCategoria, Integer stock ){
		this.idProducto=idProducto;
		this.nombreProducto=nombreProducto;
		this.descripcionProducto=descripcionProducto;
		this.imagen=imagen;
		this.precio=precio;
		this.idCategoria = idCategoria;
		this.stock = stock;
		
	}
	
	public TopMasVendido(String nombreProducto, String descripcionProducto,		
			String imagen, Double precio,Categoria idCategoria, Integer stock ){
		this.nombreProducto=nombreProducto;
		this.descripcionProducto=descripcionProducto;
		this.imagen=imagen;
		this.precio=precio;
		this.idCategoria = idCategoria;
		this.stock = stock;
		
	}

	public Integer getIdProducto() {
		return idProducto;
	}


	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}


	public String getNombreProducto() {
		return nombreProducto;
	}


	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}


	public Double getPrecio() {
		return precio;
	}


	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	


	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	public Integer getStock() {
		return stock;
	}
	
	
	public Categoria getIdCategoria() {
		return idCategoria;
	}


	public void setIdCategoria(Categoria idCategoria) {
		this.idCategoria = idCategoria;
	}


	public String getDescripcionProducto() {
		return descripcionProducto;
	}


	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}


	public String getImagen() {
		return imagen;
	}


	public void setImagen(String imagen) {
		this.imagen = imagen;
	}




}

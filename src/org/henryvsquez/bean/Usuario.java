package org.henryvsquez.bean;
import org.henryvsquez.bean.Rol;


public class Usuario {
	private Integer idUsuario;
	private String nombreUsuario;
	private String apellidoUsuario;
	private String direccion;
	private String correo;
	private String telefono;
	private String nit;
	private String nick;
	private String contrasena;
	private Rol idRol;
	
	
	public Usuario() {
	}
	
	public Usuario(Integer idUsuario) {
		
		this.idUsuario = idUsuario;
		
	}


	public Usuario(Integer idUsuario, String nombreUsuario, String apellidoUsuario, String direccion, String correo,
			String telefono, String nit, String nick, String contrasena, Rol idRol) {
		super();
		this.idUsuario = idUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidoUsuario = apellidoUsuario;
		this.direccion = direccion;
		this.correo = correo;
		this.telefono = telefono;
		this.nit = nit;
		this.nick = nick;
		this.contrasena = contrasena;
		this.idRol = idRol;
	}
	

	public Usuario(Integer idUsuario, String nit) {
		
		this.idUsuario = idUsuario;
		this.nit = nit;
	}
	
	public Integer getIdUsuario() {
		return idUsuario;
	}


	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getNombreUsuario() {
		return nombreUsuario;
	}


	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}


	public String getApellidoUsuario() {
		return apellidoUsuario;
	}


	public void setApellidoUsuario(String apellidoUsuario) {
		this.apellidoUsuario = apellidoUsuario;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getCorreo() {
		return correo;
	}


	public void setCorreo(String correo) {
		this.correo = correo;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getNit() {
		return nit;
	}


	public void setNit(String nit) {
		this.nit = nit;
	}


	public Rol getIdRol() {
		return idRol;
	}


	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}


	public String getNick() {
		return nick;
	}


	public void setNick(String nick) {
		this.nick = nick;
	}


	public String getContrasena() {
		return contrasena;
	}


	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	
	
	
	
	
	
	
}

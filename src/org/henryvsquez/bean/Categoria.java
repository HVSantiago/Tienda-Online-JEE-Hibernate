package org.henryvsquez.bean;

public class Categoria {
	private Integer idCategoria;
	private String nombreCategoria;
	private String descripcion;
	
	public Categoria(){
		
	}
	
	public Categoria(Integer idCategoria, String nombreCategoria, String descripcion){
		this.idCategoria = idCategoria;
		this.nombreCategoria = nombreCategoria;
		this.descripcion = descripcion;
	
	}
	
	public Categoria(Integer idCategoria, String nombreCategoria){
		this.idCategoria = idCategoria;
		this.nombreCategoria = nombreCategoria;
	}
	
	public Categoria(String nombreCategoria, String descripcion){
		
		this.nombreCategoria = nombreCategoria;
		this.descripcion = descripcion;
	
	}
	
	
	public Categoria(Integer idCategoria){
		this.idCategoria = idCategoria;
	}

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion=descripcion;
	}
	
	  @Override
	    public String toString(){
	        return nombreCategoria;
	    }

}

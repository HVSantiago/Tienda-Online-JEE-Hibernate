package org.henryvsquez.servlet.categoria;
import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletLoadCategory extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException{
		Integer idCategoria = Integer.valueOf(request.getParameter("idCategoria"));
		Categoria categoria = (Categoria) Conexion.getInstancia().buscar(Categoria.class, idCategoria);
		
		RequestDispatcher despachador = null;
		request.setAttribute("loadCategoria", categoria);
		despachador = request.getRequestDispatcher("view/admin/formEditCategory.jsp");
		despachador.forward(request, response);
	}

}

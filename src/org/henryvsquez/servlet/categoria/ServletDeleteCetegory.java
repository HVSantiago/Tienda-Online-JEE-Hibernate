package org.henryvsquez.servlet.categoria;

import java.io.IOException;




import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.henryvsquez.servlet.usuario.ServletLogin;
import org.henryvsquez.bean.Categoria;
import org.henryvsquez.conexion.Conexion;

import javax.servlet.ServletException;

public class ServletDeleteCetegory extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException{
		
		Integer idCategoria = Integer.valueOf(request.getParameter("idCategoria"));
		Categoria categoria = (Categoria) Conexion.getInstancia().buscar(Categoria.class, idCategoria);
		Conexion.getInstancia().defaultCategory(categoria); 
		System.out.println(categoria.getIdCategoria());
		
		if(categoria != null){
			Conexion.getInstancia().eliminar(categoria); 
			ServletLogin.getInstancia().actualiza(request, response);
			response.sendRedirect("view/admin/categoria.jsp");
		}else{
			response.sendRedirect("login.jsp");
			System.out.println("Hay problemas");
		}
		
	}

}
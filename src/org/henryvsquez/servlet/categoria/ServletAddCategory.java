package org.henryvsquez.servlet.categoria;

import java.io.IOException;


import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.henryvsquez.servlet.usuario.ServletLogin;
import org.henryvsquez.bean.Categoria;
import org.henryvsquez.conexion.Conexion;




public class ServletAddCategory extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException{
		String nombre = request.getParameter("inputNombreCategoria");
		String descripcion = request.getParameter("inputDescripcionCategoria");
		Categoria categoria = new Categoria(nombre,descripcion);
		Conexion.getInstancia().agregar(categoria);
		ServletLogin.getInstancia().actualiza(request, response);
		response.sendRedirect("view/admin/categoria.jsp");
	}

}

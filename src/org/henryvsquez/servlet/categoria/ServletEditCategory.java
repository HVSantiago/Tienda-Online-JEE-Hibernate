package org.henryvsquez.servlet.categoria;

import java.io.IOException;



import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.conexion.Conexion;
import org.henryvsquez.servlet.usuario.ServletLogin;


public class ServletEditCategory extends HttpServlet {
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Categoria categoria = new Categoria();
		categoria.setIdCategoria(Integer.valueOf(req.getParameter("inputIdCategoria")));
		categoria.setNombreCategoria(req.getParameter("inputNombreCategoria"));
		categoria.setDescripcion(req.getParameter("inputDescripcionCategoria"));
		
		
		
		Conexion.getInstancia().editar(categoria);  
		ServletLogin.getInstancia().actualiza(req, resp);
		resp.sendRedirect("view/admin/categoria.jsp");
	}
}

package org.henryvsquez.servlet.usuario;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.henryvsquez.bean.Rol;
import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;

public class ServletEditPerfil extends HttpServlet{
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Usuario usuario = new Usuario();
		usuario.setIdUsuario(Integer.valueOf(req.getParameter("inputIdUsuario")));
		usuario.setNick(req.getParameter("inputNick"));
		usuario.setContrasena(req.getParameter("inputContrasena"));
		usuario.setNombreUsuario(req.getParameter("inputNombre"));
		usuario.setApellidoUsuario(req.getParameter("inputApellido"));
		usuario.setCorreo(req.getParameter("inputCorreo"));
		usuario.setTelefono(req.getParameter("inputTelefono"));
		usuario.setDireccion(req.getParameter("inputDireccion"));
		usuario.setNit(req.getParameter("inputNit"));
		usuario.setIdRol(new Rol(2));
		
		Conexion.getInstancia().editar(usuario);
		ServletLogin.getInstancia().actualiza(req, resp);
		resp.sendRedirect("view/usuario/index.jsp");
	}

}
package org.henryvsquez.servlet.usuario;

import javax.servlet.http.HttpServlet;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Factura;
import org.henryvsquez.bean.Notificacion;
import org.henryvsquez.bean.Producto;
import org.henryvsquez.bean.TopMasVendido;
import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ServletLogin extends HttpServlet {

	private static ServletLogin instancia;
	private Integer idUsuarioLog = 0;
	
	public static ServletLogin getInstancia() {
		if (instancia == null) {
			instancia = new ServletLogin();
		}
		return instancia;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Hola La peticion fue via GET");
	}

	public void actualiza(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession httpSession = request.getSession(true);
		List<Usuario> listaUsuarios = (List<Usuario>) Conexion.getInstancia().obtenerLista("from Usuario");
		List<Categoria> listaCategorias = (List<Categoria>) Conexion.getInstancia().obtenerLista("from Categoria");
		List<Producto> listaProductos = (List<Producto>) Conexion.getInstancia().obtenerLista("from Producto");
		List<Factura> listaFacturas = (List<Factura>) Conexion.getInstancia().obtenerLista("from Factura");
		List<Notificacion> listaNotificaciones = (List<Notificacion>) Conexion.getInstancia().obtenerLista("from Notificacion");
		httpSession.setAttribute("acceso", true);
		httpSession.setAttribute("listaUsuarios", listaUsuarios);
		httpSession.setAttribute("listaCategorias", listaCategorias);
		httpSession.setAttribute("listaProductos", listaProductos);
		httpSession.setAttribute("listaFacturas", listaFacturas);
		httpSession.setAttribute("listaNotificaciones", listaNotificaciones);
		
	}
	
	
	
	public void actualizaUsuario(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession httpSession = request.getSession(true);
		List<Categoria> listaCategorias = (List<Categoria>) Conexion.getInstancia().obtenerLista("from Categoria");
		List<Producto> listaProductos = (List<Producto>) Conexion.getInstancia().obtenerLista("from Producto");
		
		List<TopMasVendido> listaMasVendidos = (List<TopMasVendido>) Conexion.getInstancia().obtenerLista("from TopMasVendido");
				
		httpSession.setAttribute("acceso",true);
		httpSession.setAttribute("listaCategorias", listaCategorias);
		httpSession.setAttribute("listaProductos", listaProductos);
		httpSession.setAttribute("listaMasVendidos", listaMasVendidos);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String inputNick = request.getParameter("inputNick");
		String inputContrasena = request.getParameter("inputContrasena");
		HttpSession httpSession = request.getSession(true);
		String referer = request.getHeader("Referer");
		System.out.println(referer);
		Usuario usuario = new Usuario();

		usuario = Conexion.getInstancia().autenticar(inputNick, inputContrasena);
		if (usuario != null) {
			if (usuario.getIdRol().getIdRol() == 1) {
				httpSession.setAttribute("acceso", true);
				httpSession.setAttribute("usuarioLogeado", usuario);
				actualiza(request, response);
				response.sendRedirect("view/admin/index.jsp");

			} else {
				idUsuarioLog = usuario.getIdUsuario();
				retornarId();
				httpSession.setAttribute("acceso", true);
				httpSession.setAttribute("usuarioLogeado", usuario);
				List<Factura> listaFacturas = (List<Factura>) Conexion.getInstancia().obtenerLista("from Factura WHERE idUsuario = '"+retornarId()+"'");
				httpSession.setAttribute("listaFacturas", listaFacturas);
				actualizaUsuario(request, response);
				response.sendRedirect("view/usuario/index.jsp");
				// REDIRECCIONAR A CLIENTE
			}
		} else {
			response.sendRedirect("error.jsp");
		}
		idUsuarioLog = usuario.getIdUsuario();
	}
	
	public Integer retornarId(){
		System.out.println("Estoy restornando " + idUsuarioLog);
		return idUsuarioLog;
	}
	
}
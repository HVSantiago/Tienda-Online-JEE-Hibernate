package org.henryvsquez.servlet.usuario;


import java.io.IOException;



import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;

public class ServletDeletePerfil extends HttpServlet{

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer idUsuario = Integer.valueOf(req.getParameter("idUsuario"));
		Usuario usuario = (Usuario) Conexion.getInstancia().buscar(Usuario.class, idUsuario);
		System.out.println(usuario.getNombreUsuario());
		System.out.println(usuario.getIdUsuario());
		
		if(usuario != null){
				resp.sendRedirect("login.jsp");
				Conexion.getInstancia().eliminar(usuario);
		}else{
			System.out.println("You have problems");
			resp.sendRedirect("login.jsp");
		}
		
	}
	
}

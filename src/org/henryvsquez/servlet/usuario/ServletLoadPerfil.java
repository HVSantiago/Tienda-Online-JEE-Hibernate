package org.henryvsquez.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;

public class ServletLoadPerfil extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Integer idUsuario = Integer.valueOf(req.getParameter("idUsuario"));
		Usuario usuario = (Usuario) Conexion.getInstancia().buscar(Usuario.class, idUsuario);
		
		
			RequestDispatcher despachador = null;
			req.setAttribute("loadPerfil", usuario);
			despachador = req.getRequestDispatcher("view/usuario/formEditPerfil.jsp");
			despachador.forward(req, resp);	
		}
		
		
	}

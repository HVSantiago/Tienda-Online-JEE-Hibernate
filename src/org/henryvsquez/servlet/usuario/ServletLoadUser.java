package org.henryvsquez.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;

public class ServletLoadUser extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Integer idUsuario = Integer.valueOf(req.getParameter("idUsuario"));
		Usuario usuario = (Usuario) Conexion.getInstancia().buscar(Usuario.class, idUsuario);
		
		if(usuario.getIdRol().getIdRol() == 1){
			HttpSession httpSession = req.getSession(true);
			httpSession.setAttribute("acceso", true);
			String denied= "No puedes editar este usuario";
			httpSession.setAttribute("Denied", denied);
			System.out.println("No puedes editar este usuario");
			resp.sendRedirect("view/admin/usuario.jsp");
		}else{
			RequestDispatcher despachador = null;
			req.setAttribute("loadUsuario", usuario);
			despachador = req.getRequestDispatcher("view/admin/formEditUser.jsp");
			despachador.forward(req, resp);	
		}
		
		
	}
}

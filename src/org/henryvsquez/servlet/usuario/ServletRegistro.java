package org.henryvsquez.servlet.usuario;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.henryvsquez.bean.Rol;
import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import org.henryvsquez.servlet.usuario.ServletLogin;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ServletRegistro extends HttpServlet {
	private Integer idUsuario = 1;
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		String inputNombre = request.getParameter("inputNombre");
		String inputApellido = request.getParameter("inputApellido");
		String inputDireccion = request.getParameter("inputDireccion");
		String inputCorreo = request.getParameter("inputCorreo");
		String inputTelefono = request.getParameter("inputTelefono");
		String inputNit = request.getParameter("inputNit");
		String inputNick = request.getParameter("inputNick");
		String inputContrasena = request.getParameter("inputContrasena");
		Integer inputRol = Integer.parseInt(request.getParameter("inputRol"));
		Integer idUsuario = getIdUsuario();
		String inputAdmin = request.getParameter("registroAdmin");
		String inputUsuario = request.getParameter("registroUsuario");
		
		Usuario usuario = new Usuario(idUsuario, inputNombre, inputApellido,inputDireccion, inputCorreo,inputTelefono,
				inputNit, inputNick, inputContrasena,new Rol(inputRol));

		Conexion.getInstancia().agregar(usuario); 

		if(inputAdmin == null){
			 
			response.sendRedirect("login.jsp");
			
		}else{
			ServletLogin.getInstancia().actualiza(request, response);
			response.sendRedirect("view/admin/usuario.jsp");
		}
		
	}
	
	public Integer getIdUsuario() {
		return idUsuario++;
	}
}

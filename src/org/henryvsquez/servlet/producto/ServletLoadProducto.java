package org.henryvsquez.servlet.producto;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Producto;
import org.henryvsquez.conexion.Conexion;

public class ServletLoadProducto extends HttpServlet{
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException{
			Integer idProducto = Integer.valueOf(request.getParameter("idProducto"));
			Producto producto = (Producto) Conexion.getInstancia().buscar(Producto.class, idProducto);
			
			RequestDispatcher despachador = null;
			request.setAttribute("loadProducto", producto);
			despachador = request.getRequestDispatcher("view/admin/formEditProducto.jsp");
			despachador.forward(request, response);
		}
}

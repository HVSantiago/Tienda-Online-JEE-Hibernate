package org.henryvsquez.servlet.producto;

import java.io.IOException;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Producto;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.henryvsquez.conexion.Conexion;
import org.henryvsquez.servlet.usuario.ServletLogin;

import javax.servlet.ServletException;


public class ServletAddProducto extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException{
		
		
		String nombre = request.getParameter("inputNombreProducto");
		String descripcion = request.getParameter("inputDescripcionProducto");
		String imagen = request.getParameter("inputImagen");
		Double precio = Double.parseDouble(request.getParameter("inputPrecio"));
		System.out.println("Aqui esta el precio"+precio);
		/*producto.setPrecio(Double.parseDouble(request.getParameter("inputPrecio")));*/
		Categoria idCategoria = new Categoria(Integer.parseInt(request.getParameter("inputIdCategoria")));
		Integer stock = Integer.parseInt(request.getParameter("inputStock"));
		Producto producto = new Producto(nombre, descripcion, imagen, precio, idCategoria, stock);
		Conexion.getInstancia().agregar(producto);
		ServletLogin.getInstancia().actualiza(request, response);
		response.sendRedirect("view/admin/producto.jsp");
	}

}

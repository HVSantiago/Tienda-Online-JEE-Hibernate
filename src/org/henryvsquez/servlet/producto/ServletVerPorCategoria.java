package org.henryvsquez.servlet.producto;

import org.henryvsquez.bean.*;
import org.henryvsquez.conexion.Conexion;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;

public class ServletVerPorCategoria extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Integer idCategoria = Integer.valueOf(request.getParameter("idCategoria"));

		List<Producto> loadProductos = (List<Producto>) Conexion.getInstancia()
				.obtenerLista("FROM Producto WHERE idCategoria = '" + idCategoria + "'");

		Categoria categoriaEncontrada = (Categoria) Conexion.getInstancia().buscar(Categoria.class, idCategoria);

		RequestDispatcher despachador = null;
		request.setAttribute("loadProductos", loadProductos);
		request.setAttribute("categoriaEncontrada", categoriaEncontrada);

		despachador = request.getRequestDispatcher("view/usuario/verCategorias.jsp");
		despachador.forward(request, response);

	}

}

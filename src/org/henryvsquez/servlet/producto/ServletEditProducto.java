package org.henryvsquez.servlet.producto;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Producto;
import org.henryvsquez.conexion.Conexion;
import org.henryvsquez.servlet.usuario.ServletLogin;


public class ServletEditProducto extends HttpServlet{
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException{
		Producto producto = new Producto();
		producto.setIdProducto(Integer.valueOf(req.getParameter("inputIdProducto")));
		producto.setNombreProducto(req.getParameter("inputNombreProducto"));
		producto.setDescripcionProducto(req.getParameter("inputDescripcionProducto"));
		producto.setImagen(req.getParameter("inputImagen"));
		producto.setPrecio(Double.parseDouble(req.getParameter("inputPrecio")));
		producto.setIdCategoria(new Categoria(Integer.parseInt(req.getParameter("inputIdCategoria"))));
		producto.setStock(Integer.parseInt(req.getParameter("inputStock")));
		
		Conexion.getInstancia().editar(producto); 
		ServletLogin.getInstancia().actualiza(req, resp);
		resp.sendRedirect("view/admin/producto.jsp");
	}
}

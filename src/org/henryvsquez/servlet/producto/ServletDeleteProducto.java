package org.henryvsquez.servlet.producto;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Producto;
import org.henryvsquez.conexion.Conexion;
import org.henryvsquez.servlet.usuario.ServletLogin;

public class ServletDeleteProducto extends HttpServlet{

	public void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		Integer idProducto = Integer.valueOf(req.getParameter("idProducto"));
		Producto producto = (Producto) Conexion.getInstancia().buscar(Producto.class, idProducto); 
		
		if(producto != null){
		    Conexion.getInstancia().eliminar(producto);
			ServletLogin.getInstancia().actualiza(req, resp);
			resp.sendRedirect("view/admin/producto.jsp");
		}else{
			System.out.println("Hay problemas");
			resp.sendRedirect("login.jsp");
		}
		
		
		
	}
	
}




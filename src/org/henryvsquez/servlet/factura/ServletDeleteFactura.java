package org.henryvsquez.servlet.factura;
import java.io.IOException;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Factura;
import org.henryvsquez.bean.Producto;
import org.henryvsquez.conexion.Conexion;
import org.henryvsquez.servlet.usuario.ServletLogin;

import javax.servlet.ServletException;

public class ServletDeleteFactura extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException{
		
		Integer idFactura = Integer.valueOf(request.getParameter("idFactura"));
		Factura factura = (Factura) Conexion.getInstancia().buscar(Factura.class, idFactura);
				
		if(factura != null){
			Conexion.getInstancia().eliminar(factura);
			ServletLogin.getInstancia().actualiza(request, response);
			response.sendRedirect("view/admin/factura.jsp");
		}else{
			response.sendRedirect("view/admin/index.jsp");
			System.out.println("Hay problemas");
		}
		
	}
}

package org.henryvsquez.servlet.factura;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.henryvsquez.bean.Detalle;
import org.henryvsquez.bean.Factura;
import org.henryvsquez.bean.Notificacion;
import org.henryvsquez.bean.Producto;
import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;
import org.henryvsquez.servlet.usuario.ServletLogin;

public class ServletComprar extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession httpsession = req.getSession();
		Usuario usuario = (Usuario) httpsession.getAttribute("usuarioLogeado");
		Integer idProducto = Integer.valueOf(req.getParameter("idProducto"));
		Integer ultimaFactura = Conexion.getInstancia().mandarFactura();
		Integer cantidad = 1;
		Producto encuentraProducto = (Producto) Conexion.getInstancia().buscar(Producto.class, idProducto);
		Factura encuentraFactura = (Factura) Conexion.getInstancia().buscar(Factura.class, ultimaFactura);
		Double precioProducto = encuentraProducto.getPrecio();
		System.out.println(usuario.getIdRol().getIdRol());
		 
		if(usuario.getIdRol().getIdRol() == 1){
			if (encuentraProducto.getStock() <= 5) {
				String mensaje = "Quedan '" + encuentraProducto.getStock() + "' unidades del producto: '"
						+ encuentraProducto.getNombreProducto() + "'";
				Notificacion notificacion = new Notificacion(encuentraProducto, mensaje);
				Conexion.getInstancia().agregar(notificacion);
				ServletLogin.getInstancia().actualiza(req, resp);
				resp.sendRedirect("view/admin/notificacion.jsp");
			} else {
				Detalle compra = new Detalle(encuentraFactura, encuentraProducto, cantidad, precioProducto);
				Conexion.getInstancia().agregar(compra);
				Conexion.getInstancia().restarCantidad(encuentraProducto, cantidad);
				ServletLogin.getInstancia().actualiza(req, resp);
				resp.sendRedirect("view/admin/catalogo.jsp");
			}

		}else{
			Detalle compra = new Detalle(encuentraFactura, encuentraProducto, cantidad, precioProducto);
			Conexion.getInstancia().agregar(compra);
			Conexion.getInstancia().restarCantidad(encuentraProducto, cantidad);
			ServletLogin.getInstancia().actualiza(req, resp);
			resp.sendRedirect("view/usuario/catalogo.jsp");
			
			
		}
		
	}
}

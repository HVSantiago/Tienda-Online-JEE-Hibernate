package org.henryvsquez.servlet.factura;


import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Factura;
import org.henryvsquez.bean.Usuario;
import org.henryvsquez.conexion.Conexion;
import org.henryvsquez.servlet.usuario.ServletLogin;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

public class ServletAddFactura extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException{
		//Obtengo los valores para crear una nueva factura
		String fechaFacturacion = request.getParameter("inputFechaFacturacion");
		Integer nit = Integer.parseInt(request.getParameter("inputNit"));
		System.out.println(nit);
		Usuario idUsuario = Conexion.getInstancia().userByNit(nit);
		
		Factura factura = new Factura(idUsuario,fechaFacturacion);
		//Crea la factura
		Conexion.getInstancia().agregar(factura);
		//Obtengo el ultimo registro de la tabla factura
		Integer idFactura =Conexion.getInstancia().mandarFactura();
		System.out.println(idFactura);
		Usuario usuario = (Usuario) Conexion.getInstancia().buscar(Usuario.class, idUsuario.getIdUsuario());
		if(usuario.getIdRol().getIdRol() == 1){
			response.sendRedirect("view/admin/catalogo.jsp");	
		}else{
			response.sendRedirect("view/usuario/catalogo.jsp");
		}
		
		
	}
	
		
}

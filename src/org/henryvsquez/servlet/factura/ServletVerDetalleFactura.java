package org.henryvsquez.servlet.factura;

import org.henryvsquez.bean.*;
import org.henryvsquez.conexion.Conexion;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;

public class ServletVerDetalleFactura extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Integer idFactura = Integer.valueOf(request.getParameter("idFactura"));

		
		List<Detalle> loadDetalles = (List<Detalle>) Conexion.getInstancia()
				.obtenerLista("FROM Detalle WHERE idFactura = '" + idFactura + "'");
		
		Factura facturaEncontrada = (Factura) Conexion.getInstancia().buscar(Factura.class, idFactura);
		Double suma = Conexion.getInstancia().obtenerSuma(idFactura); 
		
		RequestDispatcher despachador = null;
		request.setAttribute("loadDetalles", loadDetalles);
		request.setAttribute("facturaEncontrada",facturaEncontrada);
		request.setAttribute("suma",suma);
		HttpSession httpsession = request.getSession();
		Usuario usuario = (Usuario) httpsession.getAttribute("usuarioLogeado");
		
		if(usuario.getIdRol().getIdRol() == 1){
			despachador = request.getRequestDispatcher("view/admin/detalleFactura.jsp");
			despachador.forward(request, response);
		}else{
			despachador = request.getRequestDispatcher("view/usuario/detalleFactura.jsp");
			despachador.forward(request, response);
		}
		
		

	}

}

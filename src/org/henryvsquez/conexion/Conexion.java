package org.henryvsquez.conexion;

import java.util.List;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Producto;
import org.henryvsquez.bean.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;

public class Conexion {
	private static Conexion instancia;
	private SessionFactory sessionFactory;
	private Transaction transaction;
	private Session session;

	public static Conexion getInstancia() {
		if (instancia == null) {
			instancia = new Conexion();
		}
		return instancia;
	}

	public Conexion() {
		try {
			sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			System.out.println("Configuracion finalizada!");
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public Transaction getTransaccion() {
		session = sessionFactory.getCurrentSession();
		return session.getTransaction();
	}

	public void agregar(Object object) {
		transaction = getTransaccion();
		try {
			transaction.begin();
			session.save(object);
			transaction.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			transaction.rollback();
		}
	}

	public void editar(Object object) {
		transaction = getTransaccion();
		try {
			transaction.begin();
			session.update(object);
			transaction.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			transaction.rollback();
		}
	}

	public List<?> getObject(String consulta) {
		List<?> lista = null;
		transaction = getTransaccion();
		try {
			transaction.begin();
			lista = session.createQuery(consulta).list();
			transaction.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			transaction.rollback();
		}
		return lista;
	}

	public void eliminar(Object object) {
		transaction = getTransaccion();
		try {
			transaction.begin();
			session.delete(object);
			transaction.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			transaction.rollback();
		}
	}

	public Usuario autenticar(String nick, String contrasena) {
		List<Usuario> listaUsuario = null;
		Usuario usuario = null;
		transaction = getTransaccion();
		try {
			transaction.begin();
			String consulta = "FROM Usuario WHERE nick = '" + nick + "' AND contrasena = '" + contrasena + "'";
			listaUsuario = session.createQuery(consulta).list();
			transaction.commit();
			if (!listaUsuario.isEmpty()) {
				usuario = (Usuario) listaUsuario.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return usuario;
	}

	public Usuario userByNit(Integer nitUsuario) {
		List<Usuario> listaUsuario = null;
		Usuario usuario = null;
		transaction = getTransaccion();
		try {
			transaction.begin();
			String consulta = "FROM Usuario WHERE nit = '" + nitUsuario + "'";
			listaUsuario = session.createQuery(consulta).list();
			transaction.commit();
			if (!listaUsuario.isEmpty()) {
				usuario = (Usuario) listaUsuario.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}

	public void defaultCategory(Categoria categoria) {

		transaction = getTransaccion();
		try {
			transaction.begin();
			String consulta = "UPDATE Producto SET idCategoria SET = 1 WHERE idCateoria " + "= '"
					+ categoria.getIdCategoria() + "' ";
			session.createQuery(consulta);
			transaction.commit();
			System.out.println("Los productos pertenecientes a esta categoria");
			System.out.println("han sido transferidos a default categoria");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Double obtenerSuma(Integer idFactura) {

		List<Double> listaSuma = null;
		Double suma = 0.0;
		transaction = getTransaccion();
		try {
			transaction.begin();
			String consulta = "SELECT SUM(precioDetalle) FROM Detalle WHERE idFactura = '" + idFactura + "'";
			listaSuma = session.createQuery(consulta).list();
			transaction.commit();
			if (!listaSuma.isEmpty()) {
				suma = (Double) listaSuma.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return suma;

	}

	public void restarCantidad(Producto producto, Integer restar) {
		System.out.println("hola" + producto.getIdProducto());
		System.out.println("hola" + restar);
		transaction = getTransaccion();
		try {
			transaction.begin();
			/*
			 * String consulta = "UPDATE Producto SET stock = (stock - '"+
			 * restar +"') WHERE idProducto = '"+producto.getIdProducto()+"'";
			 * session.createQuery(consulta);
			 */
			Query query = session.createQuery("UPDATE Producto SET stock = (stock - '" + restar
					+ "') WHERE idProducto = '" + producto.getIdProducto() + "'");
			int result = query.executeUpdate();

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Integer mandarFactura() {
		Integer ultimaFactura = 0;
		List<Integer> ultima = null;
		transaction = getTransaccion();
		try {
			transaction.begin();
			String consulta = "SELECT MAX(idFactura) FROM Factura";
			ultima = session.createQuery(consulta).list();
			if (!ultima.isEmpty()) {
				ultimaFactura = (Integer) ultima.get(0);
			}
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ultimaFactura;
	}

	public Object buscar(Class<?> clase, Integer id) {
		Object object = null;
		transaction = getTransaccion();
		try {
			transaction.begin();
			object = session.get(clase, id);
			transaction.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			transaction.rollback();
		}
		return object;
	}

	public List<?> obtenerLista(String consulta) {
		List<?> lista = null;
		transaction = getTransaccion();
		try {
			transaction.begin();
			lista = session.createQuery(consulta).list();
			transaction.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			transaction.rollback();
		}
		return lista;
	}

}
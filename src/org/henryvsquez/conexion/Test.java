package org.henryvsquez.conexion;

import java.util.List;

import org.henryvsquez.bean.Categoria;
import org.henryvsquez.bean.Producto;
public class Test {

	public static void main(String[] args) {
		new Test();
	}
	
	public Test() {
		
		// HSQL
		List<Producto> listaProducto = (List<Producto>) Conexion.getInstancia().obtenerLista("from Producto");
		
		for(Producto Producto: listaProducto) {
			System.out.println("Id: " + Producto.getIdProducto());
			System.out.println("Nombre:" + Producto.getNombreProducto());
			System.out.println("----------------------------");
		}
		

		
	}

}
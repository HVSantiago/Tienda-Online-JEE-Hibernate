-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-03-2017 a las 07:13:02
-- Versión del servidor: 5.7.17-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.15-0ubuntu0.16.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `DatabaseInnova`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Categoria`
--

CREATE TABLE `Categoria` (
  `idCategoria` int(11) NOT NULL,
  `nombreCategoria` varchar(50) NOT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Categoria`
--

INSERT INTO `Categoria` (`idCategoria`, `nombreCategoria`, `descripcion`) VALUES
(1, 'Default', 'Sin categoria'),
(2, 'Hardware', 'Esta editada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle`
--

CREATE TABLE `Detalle` (
  `idDetalle` int(11) NOT NULL,
  `idFactura` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `cantidadDetalle` int(11) NOT NULL,
  `precioDetalle` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Detalle`
--

INSERT INTO `Detalle` (`idDetalle`, `idFactura`, `idProducto`, `cantidadDetalle`, `precioDetalle`) VALUES
(1, 8, 1, 1, '44.50'),
(2, 8, 2, 1, '33.23'),
(3, 8, 1, 1, '44.50'),
(4, 8, 2, 1, '33.23'),
(5, 8, 1, 1, '44.50'),
(6, 8, 2, 1, '33.23'),
(7, 9, 1, 1, '44.50'),
(8, 9, 1, 1, '44.50'),
(9, 9, 1, 1, '44.50'),
(10, 9, 1, 1, '44.50'),
(11, 9, 1, 1, '44.50'),
(12, 10, 1, 1, '44.50'),
(13, 10, 1, 1, '44.50'),
(14, 10, 1, 1, '44.50'),
(15, 10, 2, 1, '33.23'),
(16, 10, 2, 1, '33.23'),
(17, 11, 1, 1, '44.50'),
(18, 12, 1, 1, '44.50'),
(19, 12, 2, 1, '33.23'),
(20, 13, 1, 1, '44.50'),
(21, 16, 3, 1, '26000.00'),
(22, 17, 1, 1, '44.50'),
(23, 17, 1, 1, '44.50'),
(24, 18, 2, 1, '33.23'),
(25, 18, 2, 1, '33.23'),
(26, 18, 2, 1, '33.23'),
(27, 18, 1, 1, '44.50'),
(28, 18, 1, 1, '44.50'),
(29, 18, 3, 1, '26000.00'),
(30, 18, 3, 1, '26000.00'),
(31, 18, 1, 1, '44.50'),
(32, 18, 1, 1, '44.50'),
(33, 19, 2, 1, '33.23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Factura`
--

CREATE TABLE `Factura` (
  `idFactura` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `fechaFacturacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Factura`
--

INSERT INTO `Factura` (`idFactura`, `idUsuario`, `fechaFacturacion`) VALUES
(1, 2, '2017-02-14'),
(8, 5, '0012-12-12'),
(9, 5, '0004-04-04'),
(10, 5, '0003-03-03'),
(11, 5, '2017-02-22'),
(12, 5, '2017-01-01'),
(13, 5, '0444-04-04'),
(14, 5, '2017-03-15'),
(16, 5, '0033-03-22'),
(17, 6, '2017-03-01'),
(18, 6, '2017-03-01'),
(19, 5, '0002-02-02');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `facturas`
--
CREATE TABLE `facturas` (
`idFactura` int(11)
,`idUsuario` int(11)
,`nit` int(9)
,`fechaFacturacion` date
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Notificacion`
--

CREATE TABLE `Notificacion` (
  `idNotificacion` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `notificacion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Notificacion`
--

INSERT INTO `Notificacion` (`idNotificacion`, `idProducto`, `notificacion`) VALUES
(1, 1, 'Quedan \'5\' unidades del producto: \'Cable UTP\''),
(2, 2, 'Quedan \'5\' unidades del producto: \'Hp\'');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Producto`
--

CREATE TABLE `Producto` (
  `idProducto` int(11) NOT NULL,
  `nombreProducto` varchar(30) NOT NULL,
  `descripcionProducto` varchar(200) DEFAULT NULL,
  `imagen` text,
  `precio` decimal(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Producto`
--

INSERT INTO `Producto` (`idProducto`, `nombreProducto`, `descripcionProducto`, `imagen`, `precio`, `stock`, `idCategoria`) VALUES
(1, 'Cable UTP', 'El mejor cable', 'http://okhosting.com/resources/uploads/2016/01/servidor.jpg', '44.50', 5, 1),
(2, 'Hp', 'aasdf', 'http://okhosting.com/resources/uploads/2016/01/servidor.jpg', '33.23', 5, 1),
(3, 'Servidor', '5000 Gb', 'http://okhosting.com/resources/uploads/2016/01/servidor.jpg', '26000.00', -1, 2);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `productos`
--
CREATE TABLE `productos` (
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE `Rol` (
  `idRol` int(11) NOT NULL,
  `nombreRol` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`idRol`, `nombreRol`) VALUES
(1, 'Admin'),
(2, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Stock`
--

CREATE TABLE `Stock` (
  `idStock` int(11) NOT NULL,
  `cantidadStock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `TopMasVendido`
--
CREATE TABLE `TopMasVendido` (
`idProducto` int(11)
,`nombreProducto` varchar(30)
,`descripcionProducto` varchar(200)
,`imagen` text
,`precio` decimal(10,2)
,`idCategoria` int(11)
,`stock` int(11)
,`masVendido` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE `Usuario` (
  `idUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(30) NOT NULL,
  `apellidoUsuario` varchar(30) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `nit` int(9) NOT NULL,
  `contrasena` varchar(30) NOT NULL,
  `nick` varchar(30) NOT NULL,
  `idRol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`idUsuario`, `nombreUsuario`, `apellidoUsuario`, `direccion`, `correo`, `telefono`, `nit`, `contrasena`, `nick`, `idRol`) VALUES
(1, 'Henry', 'Vasquez', 'Villa Lobos', 'henry@archangelsystems.com', 'henry@archangelsystems.com', 12345678, '12', 'Hacker', 1),
(2, 'Santiago', 'Alvizures', 'Villa Lobos', 'henryvsquez42@gmail.com', '44785379', 12345678, 'Hacker42', 'SantiagoAV', 2),
(5, 'Tatiana', 'Juarez', 'Villa Lobos ', 'tjuarez@gmail.com', 'tjuarez@gmail.com', 44, 'Taty12', 'Hacker42', 1),
(6, 'Henry', 'Vasquez', 'aaaaaaaaaaaaaaaaaaaaaaa', 'henryvsquez@gmail.com', 'henryvsquez@gmail.com', 234455789, 'Oficial', 'Oficial', 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `facturas`
--
DROP TABLE IF EXISTS `facturas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `facturas`  AS  select `f`.`idFactura` AS `idFactura`,`f`.`idUsuario` AS `idUsuario`,`u`.`nit` AS `nit`,`f`.`fechaFacturacion` AS `fechaFacturacion` from (`Factura` `f` join `Usuario` `u` on((`f`.`idUsuario` = `u`.`idUsuario`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `productos`
--
DROP TABLE IF EXISTS `productos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `productos`  AS  select `p`.`idProducto` AS `idProducto`,`p`.`nombreProducto` AS `nombreProducto`,`p`.`descripcionProducto` AS `descripcionProducto`,`p`.`imagen` AS `imagen`,`p`.`precio` AS `precio`,`p`.`idCategoria` AS `idCategoria`,`c`.`nombreCategoria` AS `nombreCategoria`,`p`.`idStock` AS `idStock`,`s`.`cantidadStock` AS `cantidadStock` from ((`Producto` `p` join `Categoria` `c` on((`p`.`idCategoria` = `c`.`idCategoria`))) join `Stock` `s` on((`p`.`idStock` = `s`.`idStock`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `TopMasVendido`
--
DROP TABLE IF EXISTS `TopMasVendido`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `TopMasVendido`  AS  select `Detalle`.`idProducto` AS `idProducto`,`p`.`nombreProducto` AS `nombreProducto`,`p`.`descripcionProducto` AS `descripcionProducto`,`p`.`imagen` AS `imagen`,`p`.`precio` AS `precio`,`p`.`idCategoria` AS `idCategoria`,`p`.`stock` AS `stock`,sum(`Detalle`.`idProducto`) AS `masVendido` from ((`Detalle` join `Producto` `p` on((`Detalle`.`idProducto` = `p`.`idProducto`))) join `Categoria` `c` on((`p`.`idCategoria` = `c`.`idCategoria`))) group by `Detalle`.`idProducto` order by `masVendido` desc ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Categoria`
--
ALTER TABLE `Categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `Detalle`
--
ALTER TABLE `Detalle`
  ADD PRIMARY KEY (`idDetalle`),
  ADD KEY `Detalle_ibfk_1` (`idFactura`),
  ADD KEY `Detalle_ibfk_2` (`idProducto`);

--
-- Indices de la tabla `Factura`
--
ALTER TABLE `Factura`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `Factura_ibfk_usuari_1` (`idUsuario`);

--
-- Indices de la tabla `Notificacion`
--
ALTER TABLE `Notificacion`
  ADD PRIMARY KEY (`idNotificacion`),
  ADD KEY `idProducto` (`idProducto`);

--
-- Indices de la tabla `Producto`
--
ALTER TABLE `Producto`
  ADD PRIMARY KEY (`idProducto`),
  ADD KEY `idCategoria` (`idCategoria`) USING BTREE;

--
-- Indices de la tabla `Rol`
--
ALTER TABLE `Rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `Stock`
--
ALTER TABLE `Stock`
  ADD PRIMARY KEY (`idStock`);

--
-- Indices de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `idRol` (`idRol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Categoria`
--
ALTER TABLE `Categoria`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `Detalle`
--
ALTER TABLE `Detalle`
  MODIFY `idDetalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `Factura`
--
ALTER TABLE `Factura`
  MODIFY `idFactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `Notificacion`
--
ALTER TABLE `Notificacion`
  MODIFY `idNotificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `Producto`
--
ALTER TABLE `Producto`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Rol`
--
ALTER TABLE `Rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `Stock`
--
ALTER TABLE `Stock`
  MODIFY `idStock` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Detalle`
--
ALTER TABLE `Detalle`
  ADD CONSTRAINT `Detalle_ibfk_1` FOREIGN KEY (`idFactura`) REFERENCES `Factura` (`idFactura`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Detalle_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `Producto` (`idProducto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Factura`
--
ALTER TABLE `Factura`
  ADD CONSTRAINT `Factura_ibfk_usuari_1` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_factura` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Notificacion`
--
ALTER TABLE `Notificacion`
  ADD CONSTRAINT `Notificacion_ibfk_1` FOREIGN KEY (`idProducto`) REFERENCES `Producto` (`idProducto`);

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`idRol`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
var Compra = {
	init : function() {
		$(document).ready(function() {
			$("#formularioCompra").hide();
			console.log("Pagina Cargada!");
			
		});
	},
	mostrarForm : function() {
		$("#tablaCompra").hide(1000);
		$("#formularioCompra").show(1000);
	}
}

var date = new Date();

var day = date.getDate();
var month = date.getMonth() + 1;
var year = date.getFullYear();

if (month < 10) month = "0" + month;
if (day < 10) day = "0" + day;

var today = year + "-" + month + "-" + day;       
document.getElementById("theDate").value = today;

Compra.init();

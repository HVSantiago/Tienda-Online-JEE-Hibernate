
<%@ include file="navbar.jsp"%>


<h1 class="page-header">Dashboard</h1>
<div class="containes">
	<div class="section">
		<div class="row">
			<div class="col s3"></div>
			<div class="col s4">
				<ul class="collection">
					<%
						int contador = 0;
					%>
					<c:forEach var="producto" items="${listaProductos}">
						<%
							contador++;
								if ((contador % 2) != 0) {
						%>
						<li class="collection-item"><c:forEach var="categoria"
								items="${listaCategorias}">
								<c:if
									test="${categoria.getIdCategoria() == producto.getIdCategoria().getIdCategoria()}">
									<div class="row">
										<div class="col s12">
											<div class="card">
												<div class="card-image">
													<img src="${producto.getImagen()}" /> <a
														class="activator horizontal btn-floating btn-small halfway-fab waves-effect green accent-4"
														style="position: absolute; left: 220px; top: 180px; z-index: 3;">
														<i class="material-icons">play_arrow</i>
													</a>
												</div>
												<div class="card-content">
													<p>${producto.getDescripcionProducto()}</p>
												</div>
												<div class="card-reveal">
													<span class="card-title grey-text text-darken-4">${producto.getNombreProducto()}<i
														class="material-icons right">close</i></span>
													<p>Categoria: ${categoria.getNombreCategoria()}</p>
													<p>Precio: ${producto.getPrecio()}</p>
												</div>
											</div>
										</div>
									</div>
								</c:if>
							</c:forEach></li>
						<%
							}
						%>
					</c:forEach>
				</ul>
			</div>

			<div class="col s4">
				<ul class="collection">
					<%
						int contador2 = 0;
					%>
					<c:forEach var="producto" items="${listaProductos}">
						<%
							contador2++;
								if ((contador2 % 2) == 0) {
						%>
						<li class="collection-item"><c:forEach var="categoria"
								items="${listaCategorias}">
								<c:if
									test="${categoria.getIdCategoria() == producto.getIdCategoria().getIdCategoria()}">
									<div class="row">
										<div class="col s12">
											<div class="card">
												<div class="card-image">
													<img src="${producto.getImagen()}" /> <a
														class="activator horizontal btn-floating btn-small halfway-fab waves-effect green accent-4"
														style="position: absolute; left: 220px; top: 180px; z-index: 3;">
														<i class="material-icons">play_arrow</i>
													</a>
												</div>
												<div class="card-content">
													<p>${producto.getDescripcionProducto()}</p>
												</div>
												<div class="card-reveal">
													<span class="card-title grey-text text-darken-4">${producto.getNombreProducto()}<i
														class="material-icons right">close</i></span>
													<p>Categoria: ${categoria.getNombreCategoria()}</p>
													<p>Precio: ${producto.getPrecio()}</p>
												</div>
											</div>
										</div>
									</div>
								</c:if>
							</c:forEach></li>
						<%
							}
						%>
					</c:forEach>
				</ul>
			</div>
		</div>

	</div>
</div>
</div>
<%@ include file="footer.jsp"%>
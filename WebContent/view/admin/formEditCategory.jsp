<%@ include file="navEdit.jsp" %>
<div class="col-sm-9 col-ms-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Categoria</h1>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h2>Actualizar categoria</h2>
			<form action="ServletEditCategory.do" method="post">
				<input type="hidden" name="inputIdCategoria"
					value="${loadCategoria.getIdCategoria()}">
				<div class="form-group">
					<label>Nombre de Categoriak</label> <input type="text" class="form-control"
						placeholder="Nombre de Categoria" name="inputNombreCategoria" required="required"
						value="${loadCategoria.getNombreCategoria()}">
				</div>
				<div class="form-group">
					<label>Descripcion</label> <input type="text"
						class="form-control" placeholder="Introduzca una corta descripcion"
						name="inputDescripcionCategoria" required="required"
						value="${loadCategoria.getDescripcion()}">
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg">Actualizar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="footerEdit.jsp" %>
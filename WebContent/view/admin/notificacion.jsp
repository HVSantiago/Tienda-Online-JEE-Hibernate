<%@ include file="navbar.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<div class="page-header">
		<div class="pull-left">
			<h1>Notificaciones de Stock</h1>
		</div>
		<div class="clearfix"></div>
	</div>

<c:forEach var="notificacion" items="${listaNotificaciones}">
	<div class="alert alert-danger" role="alert">${notificacion.getNotificacion()}</div>
</c:forEach>	

</div>


<%@ include file="footer.jsp"%>
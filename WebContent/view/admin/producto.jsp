<%@ include file="navbar.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Productos</h1>
	<div class="row" id="tablaProducto">
		<div class="col-md-12">
			<button class="btn btn-success" onclick="Producto.mostrarForm()">Nuevo
				Producto</button>
			<table  class="table table-hover">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombre Producto</th>
						<th>Descripcion</th>
						<th>Imagen</th>
						<th>Precio</th>
						<th>Categoria</th>
						<th>Stock</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="producto" items="${listaProductos}">
						<tr>
							<td>${producto.getIdProducto()}</td>
							<td>${producto.getNombreProducto()}</td>
							<td>${producto.getDescripcionProducto()}</td>
							<td><img src="${producto.getImagen()}" alt="Smiley face"
								height="42" width="42"></td>
							<td>${producto.getPrecio()}</td>
							<td>${producto.getIdCategoria().getNombreCategoria()}</td>
							<td><input id="st" type="text" value="${producto.getStock()}" disabled ></td>
							<td>
								<div class="btn-group">
									<a class="btn btn-info"
										href="../../ServletLoadProducto.do?idProducto=${producto.getIdProducto()}">Editar</a>
									<a class="btn btn-danger"
										href="../../ServletDeleteProducto.do?idProducto=${producto.getIdProducto()}">Eliminar</a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" id="formularioProducto">
		<div class="col-md-4 col-md-offset-4">
			<h2>Formulario producto</h2>
			<form action="../../ServletAddProducto.do" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nombre del Producto</label> <input type="text"
						class="form-control" placeholder="Nombre del Producto"
						name="inputNombreProducto" required="required">
				</div>
				<div class="form-group">
					<label>Descripcion</label> <input type="text"
						class="form-control"
						placeholder="Introduzca una corta descripcion"
						name="inputDescripcionProducto" required="required">
				</div>
				<div class="form-group">
					<label>Photo</label> <label class="control-label">Select
						File</label> <input id="input-1a" type="file" accept="image/*"
						placeholder="Seleccione una imagen" class="file"
						name="inputImagen" data-show-preview="true" required="required">
				</div>

				<div class="form-group">
					<label>Precio</label> <input type="text" class="form-control"
						placeholder="Presion en Quetzales" name="inputPrecio" required="required">
				</div>


				<div class="form-group">
					<div class="input-field">
						<label>Categoria</label> <select
							name="inputIdCategoria" class="form-control">
							<option value="" disabled selected>Seleccione una categoria</option>
							<c:forEach var="categoria" items="${listaCategorias}">
								<option value="${categoria.getIdCategoria()}">${categoria.getNombreCategoria()}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label>Cantidad de Productos</label> <input type="text"
						class="form-control"
						placeholder="Introduzca la cantidad de productos (solo enteros)"
						name="inputStock" required="required">
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg">Agregar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="footer.jsp"%>
<%@ include file="navEdit.jsp"%>

<div class="col-sm-9 col-ms-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Producto</h1>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h2>Actualizar producto</h2>
			<form action="ServletEditProducto.do" method="post">
				<input type="hidden" name="inputIdProducto"
					value="${loadProducto.getIdProducto()}">
				<div class="form-group">
					<label>Nombre del Producto</label> <input type="text"
						class="form-control" placeholder="Nombre del Producto"
						name="inputNombreProducto" required="required"
						value="${loadProducto.getNombreProducto()}">
				</div>
				<div class="form-group">
					<label>Descripcion</label> <input type="text" class="form-control"
						placeholder="Introduzca una corta descripcion"
						name="inputDescripcionProducto" required="required"
						value="${loadProducto.getDescripcionProducto()}">
				</div>

				<div class="form-group">
					<label>Photo</label> <label class="control-label">Select
						File</label> <input id="input-1a" type="file" accept="image/*"
						placeholder="Seleccione una imagen" class="file"
						name="inputImagen" data-show-preview="true" required="required"
						value="${loadProducto.getImagen()}">
				</div>

				<div class="form-group">
					<label>Precio</label> <input type="text" class="form-control"
						placeholder="Presion en Quetzales" name="inputPrecio"
						required="required" value="${loadProducto.getPrecio()}">
				</div>


				<div class="form-group">
					<div class="input-field">
						<label>Categoria</label> <select name="inputIdCategoria"
							class="form-control">
							<option value="${loadProducto.getIdCategoria().getIdCategoria()}">${loadProducto.getIdCategoria().getNombreCategoria()}</option>
							<c:forEach var="categoria" items="${listaCategorias}">
								<option value="${categoria.getIdCategoria()}">${categoria.getNombreCategoria()}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label>Cantidad de Productos</label> <input type="text"
						class="form-control"
						placeholder="Introduzca la cantidad de productos (solo enteros)"
						name="inputStock" required="required">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg">Actualizar</button>
				</div>
			</form>
		</div>
	</div>
</div>



<%@ include file="footerEdit.jsp"%>
<%@ include file="navbar.jsp" %>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Categorias</h1>
  <div class="row" id="tablaCategoria">
  	<div class="col-md-12">
  		<button class="btn btn-success"  onclick="Categoria.mostrarForm()">Nueva Categoria</button>
  		<table class="table table-hover">
  			<thead>
  				<tr>
  					<th>Id</th>
  					<th>Nombre</th>
  					<th>Descripcion</th>
  					<th>Opciones</th>
  				</tr>
  			</thead>
  			<tbody>
  				<c:forEach var="categoria" items="${listaCategorias}">
  					<tr>
  						<td>${categoria.getIdCategoria()}</td>
  						<td>${categoria.getNombreCategoria()}</td>
  						<td>${categoria.getDescripcion()}</td>
  						<td>
  							<div class="btn-group">
  								<a class="btn btn-info" href="../../ServletLoadCategory.do?idCategoria=${categoria.getIdCategoria()}">Editar</a>
  								<a class="btn btn-danger" href="../../ServletDeleteCetegory.do?idCategoria=${categoria.getIdCategoria()}">Eliminar</a>
  							</div>
  						</td>
  					</tr>
  				</c:forEach>
  			</tbody>
  		</table>
  	</div>
  </div>
  <div class="row" id="formularioCategoria">
  	<div class="col-md-4 col-md-offset-4">
  		<h2>Formulario Categoria</h2>
  		<form action="../../ServletAddCategory.do" method="post" >
		<div class="form-group">
						<label>Nombre de Categoria</label>
						<input type="text" class="form-control" placeholder="Nombre de Categoria" name="inputNombreCategoria" required="required">
					</div>
					<div class="form-group">
						<label>Descripcion</label>
						<input type="text" class="form-control" placeholder="Introduce una peque�a descripcion" name="inputDescripcionCategoria" required="required">
					</div>
					<div class="form-group">
						<button  type="submit" class="btn btn-primary btn-lg">Agregar</button>
					</div>
		</form>
  	</div>
  </div>
</div>



<%@ include file="footer.jsp" %>




<%@ include file="navbar.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Facturas</h1>
	<div class="row" id="tablaFactura">
		<div class="col-md-12">
			<button class="btn btn-success" onclick="Factura.mostrarForm()">Nueva
				Facturacion</button>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>No. Factura</th>
						<th>Nit Usuario</th>
						<th>Fecha de facturacion</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="factura" items="${listaFacturas}">
						<tr>
							<td>${factura.getIdFactura()}</td>
							<td>${factura.getIdUsuario().getNit()}</td>
							<td>${factura.getFechaFacturacion()}</td>
							
							<td>
								<div class="btn-group">
									<a class="btn btn-success"
										href="../../ServletVerDetalleFactura.do?idFactura=${factura.getIdFactura()}">Ver</a>
									<a class="btn btn-info"
										href="../../ServletLoadDetalleFactura.do?idFactura=${factura.getIdFactura()}">Editar</a>
									<a class="btn btn-danger"
										href="../../ServletDeleteFactura.do?idFactura=${factura.getIdFactura()}">Eliminar</a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" id="formularioFactura">
		<div class="col-md-4 col-md-offset-4">
			<h2>Nueva Factura</h2>
			<form action="../../ServletAddFactura.do" method="post">
				<div class="form-group">
					<input type="date" name="inputFechaFacturacion"
						placeholder="aaaa-mm-dd">
				</div>
				<div class="form-group">
					<label>Busqueda por Nit</label> <input type="text" id="myInput"
						class="form-control" placeholder="Introduzca el nit"
						onkeyup="myFunction()" title="Type in a Nit" name="inputNit">
					<table class="table table-hover" id="myTable">
						<thead>
							<tr>
								<th>Nit</th>
								<th>Nombre</th>
								<th>Apellido</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="usuario" items="${listaUsuarios}">
								<tr>
									<td>${usuario.getNit()}</td>
									<td>${usuario.getNombreUsuario()}</td>
									<td>${usuario.getApellidoUsuario()}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg">Continuar</button>
				</div>
			</form>
		</div>
	</div>
</div>


<%@ include file="footer.jsp"%>
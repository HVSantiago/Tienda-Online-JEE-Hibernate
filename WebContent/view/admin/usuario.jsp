<%@ include file="navbar.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <h1 class="page-header">Usuario</h1>
  <div class="row" id="tablaUsuario">
  	<div class="col-md-12">
  		<button class="btn btn-success"  onclick="UsuarioCliente.mostrarForm()">Nuevo Usuario</button>
  		<table class="table table-hover">
  			<thead>
  				<tr>
  					<th>Id</th>
  					<th>Nombre</th>
  					<th>Apellido</th>
  					<th>Direccion</th>
  					<th>Correo</th>
  					<th>Telefono</th>
  					<th>Nit</th>
  					<th>Nick</th>
  					<th>Contraseņa</th>
  					<th>Opciones</th>
  				</tr>
  			</thead>
  			<tbody>
  				<c:forEach var="usuario" items="${listaUsuarios}">
  					<tr>
  						<td>${usuario.getIdUsuario()}</td>
  						<td>${usuario.getNombreUsuario()}</td>
  						<td>${usuario.getApellidoUsuario()}</td>
  						<td>${usuario.getDireccion()}</td>
  						<td>${usuario.getCorreo()}</td>
  						<td>${usuario.getTelefono()}</td>
  						<td>${usuario.getNit()}</td>
  						<td>${usuario.getNick()}</td>
  						<td>${usuario.getContrasena()}</td>
  						<td>
  							<div class="btn-group">
  								<a class="btn btn-info" href="../../ServletLoadUser.do?idUsuario=${usuario.getIdUsuario()}">Editar</a>
  								<a class="btn btn-danger" href="../../ServletDeleteUser.do?idUsuario=${usuario.getIdUsuario()}" >Eliminar</a>
  							</div>
  						</td>
  					</tr>
  				</c:forEach>
  			</tbody>
  		</table>
  	</div>
  </div>
  <div class="row" id="formularioUsuario">
  	<div class="col-md-4 col-md-offset-4">
  		<h2>Formulario Usuario</h2>
  		<form action="../../ServletRegistro.do" method="post" >
		<div class="form-group">
						<label>Nick</label>
						<input type="text" class="form-control" placeholder="Nick" name="inputNick" required="required">
					</div>
					<div class="form-group">
						<label>Contraseņa</label>
						<input type="password" class="form-control" placeholder="Contraseņa" name="inputContrasena" required="required">
					</div>
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" class="form-control" placeholder="Nombre" name="inputNombre" required="required">
					</div>
					<div class="form-group">
						<label>Apellido</label>
						<input type="text" class="form-control" placeholder="Apellido" name="inputApellido" required="required">
					</div>
					<div class="form-group">
						<label>Correo</label>
						<input type="text" class="form-control" placeholder="Correo" name="inputCorreo" required="required">
					</div>
					<div class="form-group">
						<label>Telefono</label>
						<input type="text" class="form-control" placeholder="Telefono" name="inputTelefono" required="required">
					</div>
					<div class="form-group">
						<label>Direccion</label>
						<input type="text" class="form-control" placeholder="Direccion" name="inputDireccion" required="required">
					</div>
					<div class="form-group">
						<label>Nit</label>
						<input type="text" class="form-control" placeholder="Nit" name="inputNit" required="required">
					</div>
			<div class="form-group">
				<label>Rol</label>
				<select class="form-control" name="inputRol" required="required">
					<option value="1">Administrador</option>
					<option value="2">Cliente</option>
				</select>
			</div>
			<div class="form-group">
			
				<button name="registroAdmin" value="registerAdmin" type="submit" class="btn btn-primary btn-lg">Registrar</button>
			</div>
		</form>
  	</div>
  </div>
</div>
<%@ include file="footer.jsp"%>
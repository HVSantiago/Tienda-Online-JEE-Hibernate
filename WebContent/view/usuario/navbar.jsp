<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">



<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="icon" href="../../favicon.ico">
<title>User index</title>
<!-- Bootstrap core CSS -->
<link href="../../vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Custom styles for this template -->
<link href="../../vendor/bootstrap/css/dashboard.css" rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet"
	href="../../vendor/materialize/css/materialize.min.css"
	media="screen,projection" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">VentaOnline</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.jsp">Dashboard</a></li>
					<li><a href="producto.jsp">Todos los Productos</a></li>
					<li><a href="masVendidos.jsp">Mas Vendidos</a></li>
					<li><a href="categoria.jsp">Categorias</a></li>
					<li><a href="factura.jsp">Iniciar Compra</a></li>
					<li><a href="<c:url value="../../login.jsp"/>"><i
							class="material-icons">lock_open</i></a></li>
				</ul>

			</div>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
			\
			<div class="col-sm-3 col-md-2 sidebar">
				
				<ul class="nav nav-sidebar">
					<li><a href="index.jsp">Dashboard</a></li>
					<li><a href="producto.jsp">Todos los Productos</a></li>
					<li><a href="masVendidos.jsp">Mas Vendidos</a></li>
					<li><a href="categoria.jsp">Categorias</a></li>
					<li><a href="factura.jsp">Iniciar Compra</a></li>
					<div>
					<li><a href="../../ServletLoadPerfil.do?idUsuario=${usuarioLogeado.getIdUsuario()}" 
					class="waves-effect waves-light btn"><i class="material-icons left">check</i> Editar Mi Perfil</a></li>
					</div>
					<br>
					<div>
					<li><a href="../../ServletDeletePerfil.do?idUsuario=${usuarioLogeado.getIdUsuario()}" 
					class="waves-effect waves-light btn"><i class="material-icons left">delete</i> Eliminar Cuenta</a></li>
					</div>
				</ul>
			</div>
<%@ include file="navEdit.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<div class="page-header">
		<div class="pull-left">
			<h1>No. Factura: ${facturaEncontrada.getIdFactura()}</h1>
			<h3>Usuario:</h3>
		</div>
		<div class="pull-right">
			<h3 class="text-right">No. Nit:
				${facturaEncontrada.getIdUsuario().getNit()}</h3>
			<h3 class="text-right">
				${facturaEncontrada.getIdUsuario().getNombreUsuario()}
				${facturaEncontrada.getIdUsuario().getApellidoUsuario()}</h3>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="row" id="tablaDetalle">
		<div class="col-md-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Producto</th>
						<th>Cantidad</th>
						<th>Precio</th>
						<th>Subtotal</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="detalle" items="${loadDetalles}">
						<tr>
							<td>${detalle.getIdProducto().getNombreProducto()}</td>
							<td>${detalle.getCantidadDetalle()}</td>
							<td>${detalle.getIdProducto().getPrecio()}</td>
							<td>${detalle.getPrecioDetalle()}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="alert alert-success" role="alert">Total: ${suma} </div>
			<a href="view/usuario/factura.jsp" class="btn btn-warning">CERRAR VISTA</a>
			
		</div>
	</div>	

</div>
<%@ include file="footerEdit.jsp"%>
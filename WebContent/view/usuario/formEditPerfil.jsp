<%@ include file="navEdit.jsp" %>
<div class="col-sm-9 col-ms-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Usuario</h1>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h2>Actualizar Perfil</h2>
			<form action="ServletEditPerfil.do" method="post">
				<input type="hidden" name="inputIdUsuario"
					value="${loadPerfil.getIdUsuario()}">
				<div class="form-group">
					<label>Nick</label> <input type="text" class="form-control"
						placeholder="Nick" name="inputNick" required="required"
						value="${loadPerfil.getNick()}">
				</div>
				<div class="form-group">
					<label>Contraseņa</label> <input type="password"
						class="form-control" placeholder="Contraseņa"
						name="inputContrasena" required="required"
						value="${loadPerfil.getContrasena()}">
				</div>
				<div class="form-group">
					<label>Nombre</label> <input type="text" class="form-control"
						placeholder="Nombre" name="inputNombre" required="required"
						value="${loadPerfil.getNombreUsuario()}">
				</div>
				<div class="form-group">
					<label>Apellido</label> <input type="text" class="form-control"
						placeholder="Apellido" name="inputApellido" required="required"
						value="${loadPerfil.getApellidoUsuario()}">
				</div>
				<div class="form-group">
					<label>Correo</label> <input type="text" class="form-control"
						placeholder="Correo" name="inputCorreo" required="required"
						value="${loadPerfil.getCorreo()}">
				</div>
				<div class="form-group">
					<label>Telefono</label> <input type="text" class="form-control"
						placeholder="Telefono" name="inputTelefono" required="required"
						value="${loadPerfil.getTelefono()}">
				</div>
				<div class="form-group">
					<label>Direccion</label> <input type="text" class="form-control"
						placeholder="Direccion" name="inputDireccion" required="required"
						value="${loadPerfil.getDireccion()}">
				</div>
				<div class="form-group">
					<label>Nit</label> <input type="text" class="form-control"
						placeholder="Nit" name="inputNit" required="required"
						value="${loadPerfil.getNit()}">
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg">Actualizar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="footerEdit.jsp" %>
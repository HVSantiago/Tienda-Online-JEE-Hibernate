<%@ include file="navbar.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Mis Facturas</h1>
	<div class="row"  id="tablaCompra" >
		<div class="col-md-12">
		<button class="btn btn-success" onclick="Compra.mostrarForm()">Nueva
				Facturacion</button>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>No. Factura</th>
						<th>Fecha de facturacion</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="factura" items="${listaFacturas}">
						<tr>
							<td>${factura.getIdFactura()}</td>
							<td>${factura.getFechaFacturacion()}</td>
							
							<td>
								<div class="btn-group">
									<a class="btn btn-success"
										href="../../ServletVerDetalleFactura.do?idFactura=${factura.getIdFactura()}">Ver</a>									
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" id="formularioCompra">
		<div class="col-md-4 col-md-offset-4">
			<h2>Nueva Compra</h2>
			<form action="../../ServletAddFactura.do" method="post">
				<div class="form-group">
					<input type="date" name="inputFechaFacturacion"
						placeholder="aaaa-mm-dd" id="theDate">
				</div>
				<div class="form-group">
					<label>Nit:t</label> <input type="text" 
						class="form-control" placeholder="NIT" value="${usuarioLogeado.getNit()}"
						  name="inputNit">
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg">Continuar</button>
				</div>
			</form>
		</div>
	</div>
</div>


<%@ include file="footer.jsp"%>
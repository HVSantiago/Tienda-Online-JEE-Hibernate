<%@ include file="navEdit.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<div class="page-header">
		<div class="pull-left">
			<h1>Categoria: ${categoriaEncontrada.getNombreCategoria()}</h1>
			
		</div>
		<div class="pull-right">
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="row" id="tablaCategoria">
		<div class="col-md-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Categoria</th>
						<th>Descripcion</th>
						<th>Cantidad disponible</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="producto" items="${loadProductos}">
						<tr>
							<td>${producto.getNombreProducto()}</td>
							<td>${producto.getDescripcionProducto()}</td>
							<td>${producto.getStock()}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<a href="view/usuario/categoria.jsp" class="btn btn-warning">CERRAR VISTA</a>
			
		</div>
	</div>	

</div>
<%@ include file="footerEdit.jsp"%>
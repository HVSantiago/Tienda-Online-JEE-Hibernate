<%@ include file="navbar.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Categorias</h1>
	<div class="row" id="tablaCategoria">
		<div class="col-md-12">
			
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Categoria</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="categoria" items="${listaCategorias}">
						<tr>
							
							<td>${categoria.getNombreCategoria()}</td>
							<td>
								<div class="btn-group">
									<a class="btn btn-success"
										href="../../ServletVerPorCategoria.do?idCategoria=${categoria.getIdCategoria()}">Ver</a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>


<%@ include file="footer.jsp"%>
<%@ include file="navbar.jsp" %>


<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Productos</h1>
	<div class="row" id="tablaProducto">
		<div class="col-md-12">
			<h1>Catalogo de productos</h1>
			<table  class="table table-hover">
				<thead>
					<tr>
						<th>Nombre Producto</th>
						<th>Descripcion</th>
						<th>Imagen</th>
						<th>Precio</th>
						<th>Categoria</th>
						<th>Stock</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					
					<c:forEach var="producto" items="${listaProductos}">
						<tr>
							<td>${producto.getNombreProducto()}</td>
							<td>${producto.getDescripcionProducto()}</td>
							<td><img src="${producto.getImagen()}" alt="Smiley face"
								height="42" width="42"></td>
							<td>${producto.getPrecio()}</td>
							<td>${producto.getIdCategoria().getNombreCategoria()}</td>
							<td>${producto.getStock()}</td>
							<td>
								<div class="btn-group">
									<a class="btn btn-info"
										href="../../ServletComprar.do?idProducto=${producto.getIdProducto()}">Agregar</a>
									<a class="btn btn-danger"
										href="../../ServletDescartar.do?idProducto=${producto.getIdProducto()}">Descartar</a>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

<%@ include file="footer.jsp" %>
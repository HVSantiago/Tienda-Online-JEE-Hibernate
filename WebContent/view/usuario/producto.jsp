<%@ include file="navbar.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Productos</h1>
	
	
	<div class="row" >
	<label>Busqueda por Nombre</label> <input type="text" id="myInput"
						class="form-control" placeholder="Introduzca el nombret"
						onkeyup="myFunction()" title="Type in a Nit" name="inputNombre">
		<div class="col-md-12">
			<table  class="table table-hover" id="myTable">
				<thead>
					<tr>
						<th>Nombre Producto</th>
						<th>Descripcion</th>
						<th>Imagen</th>
						<th>Precio</th>
						<th>Categoria</th>
						<th>Stock</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="producto" items="${listaProductos}">
						<tr>
							<td>${producto.getNombreProducto()}</td>
							<td>${producto.getDescripcionProducto()}</td>
							<td><img src="${producto.getImagen()}" alt="Smiley face"
								height="42" width="42"></td>
							<td>${producto.getPrecio()}</td>
							<td>${producto.getIdCategoria().getNombreCategoria()}</td>
							<td>${producto.getStock()}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
</div>
<%@ include file="footer.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Registro</title>
</head>
<body>
<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<h2>Registro</h2>
				<form action="ServletRegistro.do" method="post">
					<div class="form-group">
						<label>Nick</label>
						<input type="text" class="form-control" placeholder="Nick" name="inputNick" required="required">
					</div>
					<div class="form-group">
						<label>Contraseña</label>
						<input type="password" class="form-control" placeholder="Contraseña" name="inputContrasena" required="required">
					</div>
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" class="form-control" placeholder="Nombre" name="inputNombre" required="required">
					</div>
					<div class="form-group">
						<label>Apellido</label>
						<input type="text" class="form-control" placeholder="Apellido" name="inputApellido" required="required">
					</div>
					<div class="form-group">
						<label>Correo</label>
						<input type="text" class="form-control" placeholder="Correo" name="inputCorreo" required="required">
					</div>
					<div class="form-group">
						<label>Telefono</label>
						<input type="text" class="form-control" placeholder="Telefono" name="inputTelefono" required="required">
					</div>
					<div class="form-group">
						<label>Direccion</label>
						<input type="text" class="form-control" placeholder="Direccion" name="inputDireccion" required="required">
					</div>
					<div class="form-group">
						<label>Nit</label>
						<input type="text" class="form-control" placeholder="Nit" name="inputNit" required="required">
						<input type="hidden" class="form-control"  name="inputRol" value="2">
					</div>
					
						
					
					<div class="form-group">
						<button type="submit" name="registroUsuario" value="registerUsuario"class="btn btn-primary btn-lg">Registrar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Innova | 404 Error</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/font-awesome/css/font-awesome.css" rel="stylesheet">

</head>
<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Ups! Algo anda mal</h3>

        <div class="error-desc">
            Disculpa, parece que tus credenciales no son correctas, verifica tu contraseña y usuario. Si aùn no estas registrodo puedes hacerlo precionando este boton.
            <form class="form-inline m-t" role="form">
                
                <a href="registro.jsp" class="btn btn-primary">Registrar</a>
                
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="vendor/bootstrap/js/jquery-2.1.1.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
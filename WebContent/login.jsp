<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<h2>Login</h2>
			<form action="ServletLogin.do" method="post">
				<div class="form-group">
					<label>Nick</label>
					<input type="text" class="form-control" placeholder="Nick" name="inputNick" required="required">
				</div>			
				<div class="form-group">
					<label>Contraseña</label>
					<input type="password" class="form-control" placeholder="Contraseña" name="inputContrasena" required="required">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success btn-lg">Iniciar Sesion	</button>
					<a href="registro.jsp" class="btn btn-success btn-lg">Registrate</a>
				</div>
			</form>
			</div>
		</div>
	</div>
</body>
</html>